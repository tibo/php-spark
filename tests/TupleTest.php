<?php
namespace Cylab\Spark;

use PHPUnit\Framework\TestCase;

/**
 * @group mapreduce
 */
class TupleTest extends TestCase
{
    public function testMR()
    {
        $t = new Tuple("key", "value");
        $this->assertEquals("key", $t->key);
    }
}
