<?php
namespace Cylab\Spark;

use PHPUnit\Framework\TestCase;

/**
 * Description of DatasetTest
 *
 * @author tibo
 */
class DatasetTest extends TestCase
{
    public function testMapReduce()
    {
        $data = new Dataset([1, 2, 3, 4]);
        $this->assertEquals(20, $data
                ->map(function ($v) {
                    return 2 * $v;
                })
                ->reduce(function ($v, $agg) {
                    return $agg + $v;
                }));
    }

    public function testCount()
    {
        $data = new Dataset([1, 2, 3, 4]);
        $this->assertEquals(4, $data->count());
    }

    public function testTake()
    {
        $data = new Dataset([1, 2, 3, 4]);
        $this->assertEquals(2, count($data->take(2)));
        $this->assertEquals(1, $data->take(2)[0]);
    }

    public function testFirst()
    {
        $data = new Dataset([1, 2, 3, 4]);
        $this->assertEquals(1, $data->first());
    }

    public function testSortByKey()
    {
        $data = new Dataset(
            [new Tuple(1, 1), new Tuple(0, 0), new Tuple(3, 3), new Tuple(2, 2)]
        );
        $this->assertEquals(0, $data->sortByKey()->first()->value);
        $this->assertEquals(3, $data->sortByKey(false)->first()->value);
    }

    public function testCollect()
    {
        $data = new Dataset([1, 2, 3, 4]);
        $this->assertEquals(4, count($data->collect()));
    }

    public function testDistinct()
    {
        $data = new Dataset([1, 2, 1, 3, 4, 4]);
        $this->assertEquals(4, count($data->distinct()->collect()));
    }

    public function testGroupByKey()
    {
        $data = new Dataset([new Tuple(1, "a"), new Tuple(1, "b"), new Tuple(2, "c")]);
        $groups = $data->groupByKey();
        $this->assertEquals(1, $groups->first()->key);
        $this->assertEquals(["a", "b"], $groups->first()->value);
    }

    public function testReduceByKey()
    {
        $data = new Dataset([new Tuple(1, "a"), new Tuple(1, "b"), new Tuple(2, "c")]);
        $groups = $data->reduceByKey(function ($value, $agg) {
            return $value . $agg;
        }, "");

        $this->assertEquals("ab", $groups->first()->value);
    }
}
