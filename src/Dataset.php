<?php
namespace Cylab\Spark;

/**
 * Description of Dataset
 *
 * @author tibo
 */
class Dataset
{

    private $data;

    /**
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     *
     * @param callable $function
     * @return \Cylab\Spark\Dataset
     */
    public function map(callable $function)
    {
        return new Dataset(array_map($function, $this->data));
    }

    /**
     *
     * @param callable $function
     * @param type $initial
     * @return type
     */
    public function reduce(callable $function, $initial = null)
    {
        return array_reduce($this->data, $function, $initial);
    }

    /**
     *
     * @param callable $function
     * @param type $initial
     * @return \Cylab\Spark\Dataset
     */
    public function reduceByKey(callable $function, $initial = null)
    {
        $groups = $this->groupByKey();

        $reduced = [];
        foreach ($groups->collect() as $group_tuple) {
            $reduced[] = new Tuple(
                $group_tuple->key,
                array_reduce($group_tuple->value, $function, $initial)
            );
        }

        return new Dataset($reduced);
    }

    /**
     *
     * @param bool $ascending
     * @return \Cylab\Spark\Dataset
     */
    public function sortByKey(bool $ascending = true)
    {
        $copy = $this->data;

        if ($ascending) {
            usort($copy, function (Tuple $t1, Tuple $t2) {
                return $t1->key > $t2->key ? 1 : -1;
            });
        } else {
            usort($copy, function (Tuple $t1, Tuple $t2) {
                return $t1->key > $t2->key ? -1 : 1;
            });
        }
        return new Dataset($copy);
    }

    /**
     *
     * @return \Cylab\Spark\Dataset
     */
    public function distinct()
    {
        return new Dataset(array_unique($this->data));
    }

    /**
     * From a dataset of (K, V) tuples, returns a dataset of (K, [V]) tuples.
     * @return \Cylab\Spark\Dataset
     */
    public function groupByKey()
    {
        $groups = [];
        foreach ($this->data as $tuple) {
            $groups[$tuple->key][] = $tuple->value;
        }

        $tuples = [];
        foreach ($groups as $key => $values) {
            $tuples[] = new Tuple($key, $values);
        }
        return new Dataset($tuples);
    }

    /**
     *
     * @return array
     */
    public function collect() : array
    {
        return $this->data;
    }

    /**
     *
     * @return int
     */
    public function count() : int
    {
        return count($this->data);
    }

    /**
     *
     * @return type
     */
    public function first()
    {
        return $this->data[0];
    }

    /**
     * Return the first n elements of the dataset.
     * @param type $n
     * @return array
     */
    public function take($n = 10) : array
    {
        return array_slice($this->data, 0, $n);
    }
}
